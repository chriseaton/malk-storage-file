![malk file storage](https://gitlab.com/uploads/project/avatar/1038903/malk-storage-file.png "malk file storage")
# [malk-storage-file](https://gitlab.com/chriseaton/malk-storage-file)

This is a storage driver for [Malk](https://gitlab.com/chriseaton/malk) (content management system). It allows malk to
save models to JSON or XML files in a configurable directory.

# Usage
Using Malk, it's very easy to set your storage driver. Simply create the driver, then set the "DataDriver" field on
the server configuration.

## Easy Usage
This storage driver is compiled with Malk as the default storage driver. In your server configuration simply leave
the "Storage" option blank or set as "file".

## Custom Usage
If you want more control over the driver, you can manually set it up by calling the Malk Server's SetStorageDriver
function. For example:
```` go
    import (
        "gitlab.com/chriseaton/malk"
        "gitlab.com/chriseaton/malk-file-storage"
    )

    func main() {
        //create driver
        d := filestorage.Create("./data/", "json")
        //create server
        s := malk.CreateServer()
        //set the driver manually
        err := s.SetStorageDriver(d)
        if err != nil {
            fmt.Printf("Error setting storage driver: %s", err)
            return
        }
        //configure the server
        err = s.Configure(c)
        if err != nil {
            fmt.Printf("Error configuring the server: %s", err)
            return
        }
        //start the server
    	err = s.ListenAndServe()
    	if err != nil {
            fmt.Printf("Error starting the server: %s", err)
            return
        }
    }
````

### Package Functions

#### Create(dir, f)
dir
: *string* The directory where model data will be written too, relative to the app's root directory.
Defaults to "./data/" when blank.

f
: *string* Either "json" or "xml". Defaults to "json" when blank.

***returns***
: *FileStorageDriver, error

##### Example
```` go
d := filestorage.Create("./data/", "xml")
````

### FileStorageDriver struct

#### Fields
**Directory** *string*
  : The directory where files will be stored (needs read/write access).

**Format** *string*
: Either "json" or "xml".

**Prefix** *string*
: The prefix to every file managed by the storage driver.
