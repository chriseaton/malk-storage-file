/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk-storage-file
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package filestorage

import (
	"flag"
	"fmt"
	"gitlab.com/chriseaton/malk-storage/drivertest"
	//"io/ioutil"
	"os"
	"testing"
)

const (
	TestDataDirectory = "./test/"
)

var TestFormats = []string{"json", "xml"}

// Setup the testing environment, make sure the directory is created and exists, delete the directory when done
// unless the "-nodel" flag is specified.
func TestMain(m *testing.M) {
	noDelTestDir := flag.Bool("nodel", false, "Don't delete the test data directory when tests complete.")
	flag.Parse()
	var result int
	if err := os.Mkdir(TestDataDirectory, 0774); err != nil && !os.IsExist(err) {
		fmt.Printf("Failed to create test directory. Error: %s", err)
	} else {
		result = m.Run()
	}
	if *noDelTestDir == false {
		if err := os.RemoveAll(TestDataDirectory); err != nil && !os.IsNotExist(err) {
			fmt.Printf("Failed to delete test directory. Error: %s\n", err)
		}
	}
	os.Exit(result)
}

// Tests all the storage driver interface functions sequentially.
func TestStorageDriver(t *testing.T) {
	for _, f := range TestFormats {
		d, err := Create(TestDataDirectory, f)
		if err != nil {
			t.Errorf("Failed to create driver. Error: %s", err)
			return
		}
		drivertest.Run(d, t)
	}
}
