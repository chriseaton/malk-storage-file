/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk-storage-file
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package filestorage

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"gitlab.com/chriseaton/malk-storage"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

// A standard error that also includes an index value for work with a slice.
type SliceError struct {
	Index   int
	Message string
}

// Returns the error message
func (se *SliceError) Error() string {
	return se.Message
}

// Returns the file name of a model using it's type and ID values.
func FileName(c Configuration, entity string, id uint64) string {
	name := fmt.Sprintf("%s%s-%020d.%s", strings.ToLower(c.Prefix), strings.ToLower(entity), id, strings.ToLower(c.Format))
	return filepath.Join(c.Directory, name)
}

//Creates a new file storage driver for Malk with the specified configuration.
func Create(dir string, f string) (*Driver, error) {
	d := &Driver{
		Configuration: Configuration{
			Directory: dir,
			Format:    f,
		},
	}
	return d, nil
}

// Storage driver for Malk (CMS). Save models to your hard disk in JSON or XML format.
type Driver struct {

	//Driver configuration
	Configuration Configuration

	// The model registry
	registry *storage.Registry
}

type Configuration struct {

	// The directory where files will be stored (needs read/write access).
	Directory string

	// Either "json" or "xml".
	Format string

	// The prefix to every file managed by the storage driver.
	Prefix string
}

// Wraps the model with the actual ID value from the model for reading & writing to file.
type fileWrapper struct {
	UniqueID uint64        `xml:"ID" json:"id"`
	Model    storage.Model `xml:"Model" json:"model"`
	XMLName  struct{}      `xml:"ModelFileWrapper" json:"-"`
}

// Sets up the storage driver when the server is configured.
func (d *Driver) Configure(c storage.DriverConfiguration, r *storage.Registry) error {
	dc, ok := c.(Configuration)
	if ok {
		d.Configuration = dc
	}
	if d.Configuration.Directory == "" {
		d.Configuration.Directory = "./data/"
	}
	if d.Configuration.Format == "" {
		d.Configuration.Format = "json"
	}
	fi, err := os.Stat(d.Configuration.Directory)
	if err != nil && os.IsNotExist(err) {
		return fmt.Errorf("The specified path %q does not exist.", d.Configuration.Directory)
	} else if err != nil {
		return err
	} else if fi.IsDir() == false {
		return fmt.Errorf("The specified path %q is not a directory.", d.Configuration.Directory)
	}
	if r == nil {
		return fmt.Errorf("The registry argument must be specified.")
	}
	d.registry = r
	return nil
}

// Saves the model to a file, assigning the model a new id if necessary.
func (d *Driver) Save(entity string, m storage.Model) (uint64, error) {
	//validate entity
	if err := d.registry.Valid(entity, m); err != nil {
		return 0, err
	}
	//begin save
	var err error
	var id uint64 = m.ID()
	//assign a new ID if needed.
	if id == 0 {
		id, err = d.nextID(entity)
		if err != nil {
			return 0, err
		}
		m.SetID(id)
	}
	//write the model to file
	err = d.writeFile(FileName(d.Configuration, entity, id), m)
	return id, err
}

// Saves the slice of models to file, assigning new id's as necessary.
func (d *Driver) SaveAll(entity string, models ...storage.Model) error {
	for i, m := range models {
		_, err := d.Save(entity, m)
		if err != nil {
			return &SliceError{
				Index:   i,
				Message: fmt.Sprintf("Error saving model at index: %v.\nError: %v", i, err),
			}
		}
	}
	return nil
}

// Retrieves the model, specified by the ID, from the file system.
func (d *Driver) Get(entity string, id uint64) (storage.Model, error) {
	//validate entity
	if d.registry.Exists(entity) == false {
		return nil, fmt.Errorf(storage.ErrorEntityNotRegistered, entity)
	}
	//check if model exists
	var err error
	var m storage.Model
	//read the model from file
	m, err = d.readFile(entity, FileName(d.Configuration, entity, id))
	if err != nil && os.IsNotExist(err) {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	return m, nil
}

// Retrieves the models as specified by their particular ID from the file system.
func (d *Driver) GetAll(entity string, ids ...uint64) (models []storage.Model, err error) {
	for i, id := range ids {
		m, err := d.Get(entity, id)
		if err != nil {
			return models, &SliceError{
				Index:   i,
				Message: fmt.Sprintf("Error getting model with ID %q at index: %v.\nError: %s", id, i, err),
			}
		}
		models = append(models, m)
	}
	return models, nil
}

// Checks if an entity model exists for each id specified.
// If any id is not found, False is returned. If all ids have a matching model, True is returned.
func (d *Driver) Exists(entity string, ids ...uint64) (bool, error) {
	//validate entity
	if d.registry.Exists(entity) == false {
		return false, fmt.Errorf(storage.ErrorEntityNotRegistered, entity)
	}
	//check if the file exists
	for i, id := range ids {
		fileName := FileName(d.Configuration, entity, id)
		_, err := os.Stat(fileName)
		if err != nil && os.IsNotExist(err) {
			return false, nil
		} else if err != nil {
			return false, &SliceError{
				Index:   i,
				Message: fmt.Sprintf("Error checking if model with ID %q at index %v exists. \nError: %s", id, i, err),
			}
		}
	}
	return true, nil
}

// Searches model files for those that match the given criteria.
func (d *Driver) Query(qry *storage.Query) ([]storage.Model, error) {
	if qry == nil {
		return nil, fmt.Errorf("The \"qry\" parameter must be specified.")
	} else if err := qry.Validate(); err != nil {
		return nil, err
	} else if d.registry.Exists(qry.Entity) == false {
		return nil, fmt.Errorf(storage.ErrorEntityNotRegistered, qry.Entity)
	}
	files, err := d.filesForEntity(qry.Entity)
	if err != nil {
		return nil, err
	}
	//scan each file for matching criteria and add matched models to return slice
	var models []storage.Model
	for _, fid := range files {
		m, err := d.Get(qry.Entity, fid)
		if err != nil {
			return nil, err
		} else {
			var matches bool
			vmap, err := d.registry.FieldValueMap(qry.Entity, m)
			if err != nil {
				return nil, err
			} else {
				matches, err = qry.Eval(vmap)
			}
			if err != nil {
				return nil, err
			} else if matches {
				models = append(models, m)
			}
		}
	}
	return models, nil
}

// Searches model files and returns only the specified field values.
// Returns a QueryFieldResult map (map[uint64]map[string]interface{}) and an error if one occurred.
func (d *Driver) QueryFields(qry *storage.Query, fields ...string) (storage.QueryFieldResultMap, error) {
	if qry == nil {
		return nil, fmt.Errorf("The \"qry\" argument must be specified.")
	} else if err := qry.Validate(); err != nil {
		return nil, err
	} else if fields == nil || len(fields) == 0 {
		return nil, fmt.Errorf("The \"fields\" argument must be specified.")
	} else if d.registry.Exists(qry.Entity) == false {
		return nil, fmt.Errorf(storage.ErrorEntityNotRegistered, qry.Entity)
	}
	files, err := d.filesForEntity(qry.Entity)
	if err != nil {
		return nil, err
	}
	//scan each file for matching criteria and get field information and add to result map.
	results := make(storage.QueryFieldResultMap)
	for _, fid := range files {
		m, err := d.Get(qry.Entity, fid)
		if err != nil {
			return nil, err
		} else {
			var matches bool
			vmap, err := d.registry.FieldValueMap(qry.Entity, m)
			if err != nil {
				matches, err = qry.Eval(vmap)
			}
			if err != nil {
				return nil, err
			} else if matches {
				//matching model found, extract values requested.
				results[fid] = make(map[string]interface{})
				for _, fp := range fields {
					results[fid][fp] = vmap[fp].Value
				}
			}
		}
	}
	return results, nil
}

// Permanently deletes the model file with the specified id.
// Returns True if the file was found and deleted. Returns False if the file couldn't be deleted (not found or error)
func (d *Driver) Delete(entity string, id uint64) (bool, error) {
	//validate entity
	if d.registry.Exists(entity) == false {
		return false, fmt.Errorf(storage.ErrorEntityNotRegistered, entity)
	}
	//attempt delete
	fileName := FileName(d.Configuration, entity, id)
	err := os.Remove(fileName)
	if os.IsNotExist(err) {
		return false, nil
	} else if err != nil {
		return false, err
	}
	return true, nil
}

// Permanently deletes all model files with the matching id values. If an error occurs, a SliceError is returned
// deletion of further models is halted.
func (d *Driver) DeleteAll(entity string, ids ...uint64) error {
	for i, id := range ids {
		_, err := d.Delete(entity, id)
		if err != nil {
			return &SliceError{
				Index:   i,
				Message: fmt.Sprintf("Error deleting model with ID %q at index: %v.\nError: %s", id, i, err),
			}
		}
	}
	return nil
}

// Permanently deletes all models associated with the specified entity.
func (d *Driver) Truncate(entity string) error {
	//validate entity
	if d.registry.Exists(entity) == false {
		return fmt.Errorf(storage.ErrorEntityNotRegistered, entity)
	}
	//get all the files associated with an entity.
	files, err := d.filesForEntity(entity)
	if err != nil {
		return err
	}
	//delete the files
	for fp, _ := range files {
		err := os.Remove(fp)
		if err != nil && os.IsNotExist(err) == false {
			return err
		}
	}
	return nil
}

// Returns the next available ID for the given model type.
func (d *Driver) nextID(entity string) (uint64, error) {
	var id uint64
	files, err := d.filesForEntity(entity)
	if err != nil {
		return id, err
	}
	for _, fid := range files {
		if fid > id {
			id = fid
		}
	}
	//increment to next highest value, and return
	id++
	return id, nil
}

// Finds all file names and IDs matching the specified entity.
func (d *Driver) filesForEntity(entity string) (map[string]uint64, error) {
	output := make(map[string]uint64)
	prefix := fmt.Sprintf("%s%s-", strings.ToLower(d.Configuration.Prefix), strings.ToLower(entity))
	postfix := "." + strings.ToLower(d.Configuration.Format)
	//list files and find the highest number used.
	files, err := ioutil.ReadDir(d.Configuration.Directory)
	if err != nil {
		return nil, err
	}
	for _, file := range files {
		if file.IsDir() == false {
			name := strings.ToLower(file.Name())
			if strings.Index(name, prefix) == 0 && strings.Index(name, postfix) == len(name)-len(postfix) {
				if s, err := strconv.ParseUint(name[len(prefix):len(name)-len(postfix)], 10, 64); err == nil {
					output[file.Name()] = uint64(s)
				}
			}
		}
	}
	return output, err
}

// Writes the given model to the specified file in XML format.
func (d *Driver) writeFile(fileName string, m storage.Model) error {
	//validate args
	if fileName == "" {
		return fmt.Errorf("Argument %q must be specified.", "fileName")
	} else if m == nil {
		return fmt.Errorf("Argument %q must be specified.", "m")
	}
	//determine format
	f := "json"
	if strings.Index(strings.ToLower(fileName), ".xml") == len(fileName)-4 {
		f = "xml"
	}
	//open file
	var err error
	file, err := os.Create(fileName)
	defer file.Close()
	if err == nil {
		var b []byte
		//wrap the model in a struct that stores the ID
		wrap := &fileWrapper{UniqueID: m.ID(), Model: m}
		//marshal to bytes
		if f == "xml" {
			b, err = xml.MarshalIndent(wrap, "", "    ")
		} else {
			b, err = json.MarshalIndent(wrap, "", "    ")
		}
		if err == nil {
			_, err = file.Write(b)
		}
	}
	return err
}

// Reads a model from the specified fileName. Format is determined by file extension.
func (d *Driver) readFile(entity string, fileName string) (storage.Model, error) {
	//validate args
	if entity == "" {
		return nil, fmt.Errorf("Argument %q must be specified.", "entity")
	} else if fileName == "" {
		return nil, fmt.Errorf("Argument %q must be specified.", "fileName")
	}
	//determine format
	f := "json"
	if strings.Index(strings.ToLower(fileName), ".xml") == len(fileName)-4 {
		f = "xml"
	}
	//open file
	file, err := os.Open(fileName)
	defer file.Close()
	//create wrap object
	wrap := &fileWrapper{UniqueID: uint64(0), Model: d.registry.New(entity)}
	//decode from the file
	if err == nil {
		if f == "xml" {
			err = xml.NewDecoder(file).Decode(wrap)
		} else {
			err = json.NewDecoder(file).Decode(wrap)
		}
		if err == nil {
			wrap.Model.SetID(wrap.UniqueID)
			return wrap.Model, nil
		}
	}
	return nil, err
}
